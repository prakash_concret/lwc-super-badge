import {
    LightningElement
} from 'lwc';


// imports
export default class BoatSearch extends LightningElement {
    isLoading = false;
    boatTypeId;

    // Handles loading event
    handleLoading() {
        //Show Spinner
        this.toggleSpinner();
    }

    // Handles done loading event
    handleDoneLoading() {
        //Hide Spinner
        this.toggleSpinner();
    }

    // Handles search boat event
    // This custom event comes from the form
    searchBoats(event) {
        this.boatTypeId = event.detail.boatTypeId;
        console.log('this.boatTypeId :: ' + this.boatTypeId);

    }

    createNewBoat() {}

    toggleSpinner() {
        if (this.isLoading) {
            this.isLoading = false;
        } else {
            this.isLoading = true;
        }
    }
}