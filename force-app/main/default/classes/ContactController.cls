public with sharing class ContactController {
    
    @AuraEnabled(cacheable = true)
    public static List<Contact>  getContacts(){
        try{
            Integer i = 1/0;
            return [
                SELECT FirstName,
                LastName,
                Email
                FROM Contact
                
                WITH SECURITY_ENFORCED
            ];
            
        }catch(Exception exp ){
            throw new AuraHandledException(exp.getMessage()+'. Exception Type '+ exp.getTypeName()+ '. '+exp.getStackTraceString());
        }
        
    }
    
}